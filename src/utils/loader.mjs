const loader = (iterator, times, msg) => {
  let perc = (100 * iterator) / times

  if (iterator < 1) {
    process.stdout.write("\r\x1b[K")
    process.stdout.write(`\n[LOADER: ${msg}]\n`)
  }

  process.stdout.write("\r\x1b[K")
  process.stdout.write(`[ ${perc.toFixed(0)}% ] loaded...`)

  if (iterator + 1 === times) {
    process.stdout.write("\r\x1b[K")
    process.stdout.write("[Complete: 100%] \n")
  }
}

export default loader
