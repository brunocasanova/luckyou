export function sortResultsByHits(obj, reverse) {
  let result = obj.sort((a, b) => a.hits - b.hits)
  return reverse ? result.reverse() : result
}

export function returnBestHits(obj, val) {
  return sortResultsByHits(obj).slice(-val)
}

export function toArrayAndSort(obj) {
  return obj.map((o) => o.value).sort((a, b) => a - b)
}

export function sortArray(arr) {
  return arr.sort((a, b) => a - b)
}

export function emptyArray(arr) {
  while (arr.length > 0) {
    arr.pop()
  }
}

export function compareObjects(a, b) {
  return (
    JSON.stringify(a, Object.keys(a).sort()) ===
    JSON.stringify(b, Object.keys(b).sort())
  )
}

export function compareArrays(arr, arr2) {
  sortArray(arr)
  sortArray(arr2)

  if (!arr) return false

  if (arr2.length != arr.length) return false

  for (var i = 0, l = arr2.length; i < l; i++) {
    if (arr2[i] instanceof Array && arr[i] instanceof Array) {
      if (!compareArrays(arr[i], arr2[i])) return false
    } else if (arr2[i] != arr[i]) {
      return false
    }
  }
  return true
}
