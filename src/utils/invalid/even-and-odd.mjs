const evenAndOdd = () => {
  let five = []
  let minus = 1
  let major = 10
  let interpolate = true
  let oddKeys = []
  let evenKeys = []
  let isOdd = false

  for (let i = 0; i <= 58; i++) {
    interpolate = !interpolate

    if (i >= minus && i <= major) {
      if (interpolate) {
        five.push(i < 51 ? i : i - 50)
      }
    }

    if (five.length === 5) {
      isOdd = !isOdd
      isOdd ? oddKeys.push(five) : evenKeys.push(five)
      five = []
      minus += 1
      major += 1
      i -= 9
    }
  }

  return {
    even: evenKeys,
    odd: oddKeys,
  }
}

export default evenAndOdd
