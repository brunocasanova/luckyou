const sequential = () => {
  let invalidKeys = []
  let five = []
  let countMinus = 1
  let countMajor = 5

  for (let i = 0; i <= 54; i++) {
    if (i >= countMinus && i <= countMajor) {
      five.push(i < 51 ? i : i - 50)
    }

    if (five.length === 5) {
      invalidKeys.push(five)
      five = []

      countMinus += 1
      countMajor += 1
      i -= 4
    }
  }

  return invalidKeys
}

export default sequential
