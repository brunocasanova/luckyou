import evenAndOdd from "./even-and-odd.mjs"
import sequential from "./sequential.mjs"

export { evenAndOdd, sequential }
