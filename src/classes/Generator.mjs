import Indicators from "./Indicators.mjs"

export default class Generator {
  constructor(times) {
    // Where the object numbers and stars are going to be stored
    this.numbers = []
    this.stars = []

    // How many times is going to generate hits
    this.times = times || 1

    // let initialize the generator
    this.initialize()
  }

  initialize() {
    // Generate the base quantity of numbers and start
    this.generateBase(50, this.numbers)
    this.generateBase(12, this.stars)

    // rotates how many times that was inputed
    this.applyTimes()
  }

  generateBase(qty, arr) {
    // generate the base numbers and stars
    for (let i = 0; i < qty; i++) {
      arr.push(new Indicators(i + 1))
    }
  }

  applyTimes() {
    for (let i = 0; i < this.times; i++) {
      // Lets pick the random numbers and stars
      this.applyHits(5, this.numbers, 50)
      this.applyHits(2, this.stars, 12)
    }
  }

  applyHits(qty, arr, total) {
    let tempArr = arr
    let picked = []

    // Lets loop to pick a number/star
    for (let i = 1; i <= qty; i++) {
      let randomNr = this.getRandomNumber(picked, total)
      picked.push(randomNr)
    }

    // get the instance of the number from the picked numbers and give them a hit
    picked.forEach((nr) => tempArr.find((obj) => obj.value === nr).addHit())
  }

  getRandomNumber(arr, total) {
    // generate random number
    let randomNr = Math.floor(Math.random() * total + 1)

    // if the number exists in the numbers array lets try other
    if (arr.includes(randomNr)) {
      return this.getRandomNumber(arr, total)
    }

    return randomNr
  }
}
