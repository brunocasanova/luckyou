export default class Indicators {
  constructor(v) {
    this.value = v
    this.hits = 0
  }

  addHit() {
    this.hits += 1
  }
}
