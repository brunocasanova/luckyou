import generateKey from "./modes/generateKey.mjs"
import matchKey from "./modes/matchKey.mjs"
import generateAndMatch from "./modes/generateAndMatch.mjs"

const setup = (options) => {
  const mode = options[2]
  const times = options[3]

  switch (mode) {
    case "generate-key":
      return generateKey(times)

    case "match-key":
      const key = { numbers: options[4], stars: options[5] }
      return matchKey(key, times)

    case "generate-match":
      const matchTimes = options[4]
      return generateAndMatch(times, matchTimes)
  }
}

export default setup
