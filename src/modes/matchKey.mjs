import generateKey from "./generateKey.mjs"
import matches from "../utils/matches.mjs"
import { compareObjects } from "../utils/utils.mjs"
//import loader from "../utils/loader.mjs"

const matchKey = (key, times) => {
  for (let i = 1; i <= times; i++) {
    let generated = generateKey(1, false)
    let paired = { numbers: 0, stars: 0 }

    // Loging in terminal, only for testing
    //loader(i, times, `matchKey: testing ${times} times`)

    generated.key[0].forEach((nr) => {
      if (key.numbers.includes(nr)) {
        paired.numbers += 1
      }
    })

    generated.key[1].forEach((star) => {
      if (key.stars.includes(star)) {
        paired.stars += 1
      }
    })

    Object.keys(matches).forEach((prop) => {
      let matchList = matches[prop]

      if (compareObjects(matchList.specs, paired)) {
        matchList.hits += 1
        switch (matchList.id) {
          case 1:
          case 2:
          case 3:
            matchList.info.push(i)
            break
        }
      }
    })
  }

  Object.keys(matches).forEach((prop) => {
    const percentage = (100 * matches[prop].hits) / times
    matches[prop].percent = +percentage.toFixed(3)
  })

  return matches
}

export default matchKey
