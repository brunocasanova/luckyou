import Generator from "../classes/Generator.mjs"
import { returnBestHits, toArrayAndSort } from "../utils/utils.mjs"
import { sequential, evenAndOdd } from "../utils/invalid/index.mjs"
import { compareArrays } from "../utils/utils.mjs"

const sequentialKeys = sequential()
const evenAndOddKeys = evenAndOdd()

const generateKey = (times, notExclude) => {
  const generator = new Generator(times)
  const numbers = returnBestHits(generator.numbers, 5)
  const stars = returnBestHits(generator.stars, 2)
  let key = [toArrayAndSort(numbers), toArrayAndSort(stars)]

  if (!notExclude) {
    if (
      !excludeKeys(key[0], sequentialKeys) ||
      !excludeKeys(key[0], evenAndOddKeys.even) ||
      !excludeKeys(key[0], evenAndOddKeys.odd)
    ) {
      return generateKey(times)
    }
  }

  return { numbers, stars, key }
}

const excludeKeys = (key, invalidKeys) => {
  let resolve = true
  invalidKeys.forEach((k) => {
    if (compareArrays(key, k)) {
      resolve = false
    }
  })
  return resolve
}

export default generateKey
