import generateKey from "./generateKey.mjs"
import matchKey from "./matchKey.mjs"

const generateAndMatch = (genTimes, matchTimes) => {
  matchTimes = matchTimes || genTimes
  const { key } = generateKey(genTimes)
  const matches = matchKey({ numbers: key[0], stars: key[1] }, matchTimes)

  return { matches, key }
}

export default generateAndMatch
