import generateAndMatch from "./modes/generateAndMatch.mjs"
import loader from "./utils/loader.mjs"
let draws = []

const start = (times, genTimes) => {
  for (let i = 0; i < times; i++) {
    const genMatch = generateAndMatch(genTimes, 1)

    // Loging in terminal, only for testing
    loader(i, times, `testDraws: testing ${times} times`)

    Object.entries(genMatch.matches).forEach((m) => {
      let matchedList = m[1]

      if (matchedList.hits == 1) {
        if (matchedList.id <= 3) {
          draws.push({
            numbers: genMatch.key[0],
            stars: genMatch.key[1],
            prize: matchedList.id,
          })
        }
      }
    })
  }

  console.log("RESULT:")
  console.log(draws)
}

export default start(500, 1000)
