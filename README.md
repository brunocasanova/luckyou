# Luckyou

_Repository for study some statistics of "euromilhões" generated keys and matches of the keys with prizes._

This project runs in Node.js with the experimental-modules.

## Modes

It have 3 modes, `generate-key`, `match-key` and `generate-match`.

### Generate key

- Generates best hitted numbers and stars by a number of times.

npm run generate-key [number of times: Number]

Example:
`npm run generate-key 1000`

### Match key

- Match a key inserted and see how many matches it gets by a number of times.

npm run match-key [number of times: Number] - [key number: array of 5 numbers] - [ stars number: Array of 2 numbers]

Example:
`npm run match-key 1000 "[5,43,12,8,31]" "[2,11]"`

### Generate and Match

- Auto generates and see how many matches it gets by number of times.

npm run match-key [times of generated key: Number] - [times to match key: Number]

Example:
`npm run generate-match 1000 200`
